#!/bin/bash

# Exibe os comandos e seus argumentos à medida que são executados.
set -x 

# Grava log das operações.
mkdir -p /vagrant/provisionlog
LOGFILE="/vagrant/provisionlog/$(date +%Y%m%d_%H%M%S).log"

# Apt update
echo "Apt update...";
apt-get update >>${LOGFILE} 2>>${LOGFILE};
if ! [ $? -eq 0 ]; then echo "FAIL!"; exit 1; fi;
echo "SUCCESS";

apt-get update -y >>${LOGFILE} 2>>${LOGFILE};
if ! [ $? -eq 0 ]; then echo "FAIL!"; exit 1; fi;
echo "SUCCESS";

# Install programs
echo "Install programs...";

# Git
echo "Git...";
apt-get install git -y >>${LOGFILE} 2>>${LOGFILE};
if ! [ $? -eq 0 ]; then echo "FAIL!"; exit 1; fi;
echo "SUCCESS";

# Apache2
echo "Apache2...";
apt-get install apache2 -y >>${LOGFILE} 2>>${LOGFILE};
if ! [ $? -eq 0 ]; then echo "FAIL!"; exit 1; fi;
echo "SUCCESS";

# Php5
echo "Php5...";
apt-get install php5 -y >>${LOGFILE} 2>>${LOGFILE};
if ! [ $? -eq 0 ]; then echo "FAIL!"; exit 1; fi;
echo "SUCCESS";

# Postgresql
echo "Postgres-9.3...";
apt-get install postgresql-9.3 -y >>${LOGFILE} 2>>${LOGFILE};
if ! [ $? -eq 0 ]; then echo "FAIL!"; exit 1; fi;
echo "SUCCESS";

# Configure programs
echo "Configure programs...";

# Apache2
echo "Copy guest apache config file to host...";
cp -f /vagrant/apache2.conf /etc/apache2/apache2.conf >>${LOGFILE} 2>>${LOGFILE};
if ! [ $? -eq 0 ]; then echo "FAIL!"; exit 1; fi;
echo "SUCCESS";

echo "Copy guest apache sites available file to host...";
cp -f /vagrant/executiva.conf /etc/apache2/sites-available/executiva.conf >>${LOGFILE} 2>>${LOGFILE};
if ! [ $? -eq 0 ]; then echo "FAIL!"; exit 1; fi;
echo "SUCCESS";

echo "Symbolic linking host apache site available file to host site enabled...";
if ! [ -f  /etc/apache2/sites-enabled/executiva.conf ]; then
    ln -s /etc/apache2/sites-available/executiva.conf /etc/apache2/sites-enabled/executiva.conf >>${LOGFILE} 2>>${LOGFILE};
if ! [ $? -eq 0 ]; then echo "FAIL!"; exit 1; fi;
echo "SUCCESS";

echo "Removing host apache site default...";
if [ -f  /etc/apache2/sites-enabled/000-default.conf ]; then
    rm /etc/apache2/sites-enabled/000-default.conf >>${LOGFILE} 2>>${LOGFILE};
if ! [ $? -eq 0 ]; then echo "FAIL!"; exit 1; fi;
echo "SUCCESS";


# Php5
echo "Install php some libraries...";
apt-get install php5-dev php-pear php5-pgsql php5-memcache php5-gd php5-imagick php5-mcrypt php5-curl -y >>${LOGFILE} 2>>${LOGFILE};
if ! [ $? -eq 0 ]; then echo "FAIL!"; exit 1; fi;
echo "SUCCESS";


echo "Install xdebug...";
if [ "$(/usr/bin/pecl list | grep ^xdebug | wc -l)" -eq "0" ]; then
    pecl install xdebug >>${LOGFILE} 2>>${LOGFILE};
if ! [ $? -eq 0 ]; then echo "FAIL!"; exit 1; fi;
echo "SUCCESS";

echo "Copy guest phpini file to host...";
echo "Renew php.ini...";
cp -f /vagrant/php.ini /etc/php5/apache2/php.ini >>${LOGFILE} 2>>${LOGFILE};
if ! [ $? -eq 0 ]; then echo "FAIL!"; exit 1; fi;
echo "SUCCESS";

echo "Restart apache2 service...";
service apache2 restart >>${LOGFILE} 2>>${LOGFILE};
if ! [ $? -eq 0 ]; then echo "FAIL!"; exit 1; fi;
echo "SUCCESS";

/etc/init.d/apache2 restart >>${LOGFILE} 2>>${LOGFILE};
if ! [ $? -eq 0 ]; then echo "FAIL!"; exit 1; fi;
echo "SUCCESS";

# Postgres
echo "Create the database...";
su - postgres -c "/usr/lib/postgresql/9.3/bin/createdb executiva" >>${LOGFILE} 2>>${LOGFILE};
if ! [ $? -eq 0 ]; then echo "FAIL!"; exit 1; fi;
echo "SUCCESS";

echo "Load the database file...";
su - postgres -c "/bin/cat /vagrant/database.sql | /usr/lib/postgresql/9.3/bin/psql -d executiva" >>${LOGFILE} 2>>${LOGFILE};
if ! [ $? -eq 0 ]; then echo "FAIL!"; exit 1; fi;
echo "SUCCESS";

echo "Create user executiva...";
su - postgres -c "/usr/lib/postgresql/9.3/bin/psql -tAc \"SELECT 1 FROM pg_roles WHERE rolname='executiva'\" | grep -q 1 || /usr/lib/postgresql/9.3/bin/psql -d executiva <<EOL
	create user executiva with password 'executiva' superuser;
    alter role executiva set DateStyle = 'Ymd';
EOL" >>${LOGFILE} 2>>${LOGFILE};
if ! [ $? -eq 0 ]; then echo "FAIL!"; exit 1; fi;
echo "SUCCESS";



# installing composer

php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('SHA384', 'composer-setup.php') === '544e09ee996cdf60ece3804abc52599c22b1f40f4323403c44d44fdfdd586475ca9813a858088ffbc1f233e9b180f061') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
php -r "unlink('composer-setup.php');"

echo "Downloading the composer installer...";
php -r "readfile('https://getcomposer.org/installer');" > composer-setup.php
if ! [ $? -eq 0 ]; then echo "FAIL!"; exit 1; fi;
echo "SUCCESS";

echo "Instaling composer...";
php composer-setup.php >>${LOGFILE} 2>>${LOGFILE};
if ! [ $? -eq 0 ]; then echo "FAIL!"; exit 1; fi;
echo "SUCCESS";

echo "Removing composer installer...";
php -r "unlink('composer-setup.php');" >>${LOGFILE} 2>>${LOGFILE};
if ! [ $? -eq 0 ]; then echo "FAIL!"; exit 1; fi;
echo "SUCCESS";

echo "Put composer in the linux path...";
mv composer.phar /usr/local/bin/composer >>${LOGFILE} 2>>${LOGFILE};
if ! [ $? -eq 0 ]; then echo "FAIL!"; exit 1; fi;
echo "SUCCESS";



echo "Copy the auth.json file, to access GitHub...";
if ! [ -f /home/vagrant/.composer/auth.json ]; then
    mkdir -p /home/vagrant/.composer/
    if ! [ $? -eq 0 ]; then echo "FALHA!"; exit 1; fi;
    cp /vagrant/auth.json /home/vagrant/.composer/auth.json
if ! [ $? -eq 0 ]; then echo "FAIL!"; exit 1; fi;
echo "SUCCESS";



echo "Install composer asset plugin (required by Yii)...";
/usr/local/bin/composer global require "fxp/composer-asset-plugin:1.2.0" >>${LOGFILE} 2>>${LOGFILE};
if ! [ $? -eq 0 ]; then echo "FAIL!"; exit 1; fi;
echo "SUCCESS";

echo "Install composer dependencies...";
su - vagrant -c "/usr/local/bin/composer --working-dir=/vagrant/src install"
#>>${LOGFILE} 2>>${LOGFILE};
if ! [ $? -eq 0 ]; then echo "FAIL!"; exit 1; fi;
echo "SUCCESS";
