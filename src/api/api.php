<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require_once("../vendor/autoload.php");

$app = new \Slim\App;

$app->get("/print/{name}", function ($request, $response, $args) {
	return $response->withStatus(200)->write("Hello, ". $args["name"]);
});

$app->group("/user", function () {
    $this->get("/print/{name}", function (Request $request, Response $response, $args) {
	return $response->withStatus(200)->write("Hello, ". $args["name"]);
    });
});

$app->run();

