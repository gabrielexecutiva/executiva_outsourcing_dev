FROM ubuntu:latest
#
MAINTAINER Gabriel Henrique da Silva Dobrovolski <gabriel@executiva.adm.br>
LABEL version="1.0"
#
RUN apt-get update -y && echo "update SUCCESS"
RUN apt-get install nano -y && echo "nano SUCCESS"
RUN apt-get install git -y && echo "git SUCCESS"
RUN apt-get install apache2 -y && echo "apache2 SUCCESS"
RUN apt-get install libapache2-mod-php -y && echo "libapache2-mod-php SUCCESS"
RUN apt-get install php-xdebug -y && echo "php-xdebug SUCCESS"
RUN apt-get install php-pgsql php-dev php-pear php-memcache php-common php-cli php-gd php-imagick php-mcrypt php-curl -y && echo "php-libraries SUCCESS"
RUN apt-get install postgresql -y && echo "postgresql SUCCESS"
RUN curl -sS https://getcomposer.org/installer | php && mv composer.phar /usr/local/bin/composer
RUN apt-get autoremove -y && echo "autoremove SUCCESS"
#
COPY configuration/apache2.conf /etc/apache2/apache2.conf
COPY configuration/executiva.conf /etc/apache2/sites-available/executiva.conf
COPY configuration/php.ini /etc/php5/apache2/php.ini
#
RUN if ! [ -f /etc/apache2/sites-enabled/executiva.conf ]; then ln -s /etc/apache2/sites-available/executiva.conf /etc/apache2/sites-enabled/executiva.conf; fi
RUN if [ -f  /etc/apache2/sites-enabled/000-default.conf ]; then rm /etc/apache2/sites-enabled/000-default.conf; fi
RUN if ! [ -f  /etc/apache2/mods-enabled/rewrite.load ]; then a2enmod rewrite; fi
#
RUN echo "listen_addresses = '*'" >> /etc/postgresql/9.5/main/postgresql.conf
RUN echo "host all all 0.0.0.0/0 md5" >> /etc/postgresql/9.5/main/pg_hba.conf
#
EXPOSE 80 
EXPOSE 443
EXPOSE 5432
#
CMD service apache2 restart && service postgresql restart && su - postgres -c "/usr/lib/postgresql/9.5/bin/createdb executiva" && su - postgres -c "/usr/lib/postgresql/9.5/bin/psql -c \"ALTER USER postgres WITH PASSWORD 'postgres';\"" && su - postgres -c "/usr/lib/postgresql/9.5/bin/psql -c \"CREATE USER executiva WITH PASSWORD 'executiva' Superuser;\"" && service apache2 restart && service postgresql restart && /bin/bash
#
#
# installing composer
#php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
#php -r "if (hash_file('SHA384', 'composer-setup.php') === '544e09ee996cdf60ece3804abc52599c22b1f40f4323403c44d44fdfdd586475ca9813a858088ffbc1f233e9b180f061') { echo 'Installer verified'; } else { echo #'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
#php composer-setup.php
#php -r "unlink('composer-setup.php');"
#echo "Downloading the composer installer...";
#php -r "readfile('https://getcomposer.org/installer');" > composer-setup.php
#if ! [ $? -eq 0 ]; then echo "FAIL!"; exit 1; fi;
#echo "SUCCESS";
#echo "Instaling composer...";
#php composer-setup.php >>${LOGFILE} 2>>${LOGFILE};
#if ! [ $? -eq 0 ]; then echo "FAIL!"; exit 1; fi;
#echo "SUCCESS";
#echo "Removing composer installer...";
#php -r "unlink('composer-setup.php');" >>${LOGFILE} 2>>${LOGFILE};
#if ! [ $? -eq 0 ]; then echo "FAIL!"; exit 1; fi;
#echo "SUCCESS";
#echo "Put composer in the linux path...";
#mv composer.phar /usr/local/bin/composer >>${LOGFILE} 2>>${LOGFILE};
#if ! [ $? -eq 0 ]; then echo "FAIL!"; exit 1; fi;
#echo "SUCCESS";
#echo "Copy the auth.json file, to access GitHub...";
#if ! [ -f /home/vagrant/.composer/auth.json ]; then
#    mkdir -p /home/vagrant/.composer/
#    if ! [ $? -eq 0 ]; then echo "FALHA!"; exit 1; fi;
#    cp /vagrant/auth.json /home/vagrant/.composer/auth.json
#if ! [ $? -eq 0 ]; then echo "FAIL!"; exit 1; fi;
#echo "SUCCESS";
#echo "Install composer asset plugin (required by Yii)...";
#/usr/local/bin/composer global require "fxp/composer-asset-plugin:1.2.0" >>${LOGFILE} 2>>${LOGFILE};
#if ! [ $? -eq 0 ]; then echo "FAIL!"; exit 1; fi;
#echo "SUCCESS";
#echo "Install composer dependencies...";
#su - vagrant -c "/usr/local/bin/composer --working-dir=/vagrant/src install"
#>>${LOGFILE} 2>>${LOGFILE};
#if ! [ $? -eq 0 ]; then echo "FAIL!"; exit 1; fi;
#echo "SUCCESS";
